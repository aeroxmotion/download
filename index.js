'use strict'

/**
 * Native dependencies
 */
const { get } = require('https')
const { createWriteStream, existsSync } = require('fs')
const { dirname, resolve } = require('path')
const { parse } = require('url')
const debug = require('debug')('download')

/**
 * Vendor dependencies
 */
const { sync } = require('mkdirp')

/**
 * Download a file
 *
 * @param {string} source
 * @param {string} filename
 * @param {Object=} httpsOptions
 *
 * @return {Promise}
 */
module.exports = (source, filename, httpsOptions) => {
  if (!source.startsWith('https://')) {
    source = `https://${source}`
  }

  const destination = dirname(filename)

  if (!existsSync(destination)) {
    sync(destination)
  }

  httpsOptions = Object.assign({}, parse(source), httpsOptions)

  return new Promise((_resolve, reject) => {
    debug('Fetching file from: %s', source)

    const request = get(httpsOptions, response => {
      // TODO: Handle other status codes.
      if (response.statusCode !== 200) {
        return reject(new Error('File not found'))
      }

      const fileStream = createWriteStream(resolve(filename))

      response.on('data', chunk => {
        fileStream.write(chunk)
      })

      response.on('end', () => {
        _resolve(fileStream)
      })
    })

    request.on('error', reject)
  })
}
