'use strict'

/**
 * Vendor dependencies
 */
const test = require('tape')

/**
 * Internal dependencies
 */
const download = require('./')

test('download a file', t => {
  t.plan(1)

  download('nodejs.org/static/images/logo.svg', './img/node-logo.svg')
    .then(stream => {
      t.pass('Successful download')
    })
    .catch(({ stack }) => {
      t.fail(stack)
    })
})
