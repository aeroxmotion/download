#!/usr/bin/env node

'use strict'

/**
 * Native dependencies
 */
const { basename } = require('path')

/**
 * Internal dependencies
 */
const download = require('./')

const SKIP_INDEXES = 2

download(...process.argv.slice(SKIP_INDEXES))
  .then(({ path }) => {
    process.stdout.write(`File ${basename(path)} successfully downloaded`)
    process.exit(0)
  })
  .catch(({ stack }) => {
    process.stderr.write(stack)
    process.exit(1)
  })
