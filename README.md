# Download JS

Simple library to download a file using NodeJS.

**Note:** This library only supports `https` protocol.

## Installation

```bash
$ npm install gitlab:aeroxmotion/download
```

## Usage

### CLI

```bash
# download [source]                          [filename]
$ download nodejs.org/static/images/logo.svg ./images/node-logo.svg
```

**Note:** The `https options` are not supported.

### Programmatic

```js
const { basename } = require('path')
const download = require('download')

download('nodejs.org/static/images/logo.svg', './img/node-logo.svg', httpsOptions)
  .then(({ path }) => {
    console.log(`${basename(path)} downloaded =D`)
  })
  .catch(error => {
    console.log('Error to download :(')
    console.log(error.stack)
  })
```

## API

### download(source: string, filename: string, httpsOptions: Object) => Promise

Download a file.

**Parameters:**

- source: An url specifying the source file.
- filename: A path relative/absolute with the name of the file to download.
- httpsOptions: An object to pass to the [`https.get()`](https://nodejs.org/dist/latest-v8.x/docs/api/https.html#https_https_get_options_callback) method.

**Returns:** A promise that is resolved by passing the file ([`WritableStream`](https://nodejs.org/dist/latest-v8.x/docs/api/stream.html#stream_writable_streams))
downloaded. In case of download fail or request error the promise is rejected.
