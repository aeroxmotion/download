# Changelog

## v0.1.0 - 7/7/2017

Initial release

## v0.1.1 - 7/7/2017

  - [df4febbe] Use `source` and `destination` props.

## v0.3.0 - 9/7/2017

  - [83077fb0] Now supports `https options`.
  - [83077fb0] `destination` argument removed.

## v0.4.0 - 9/7/2017

  - [0540f3d0] Use a [`debug`](https://github.com/visionmedia/debug) utlitiy.
